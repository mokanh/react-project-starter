export default {
  SET_STATE: 'profile/SET_STATE',
  GET_PROFILE: 'profile/GET_PROFILE'
};
